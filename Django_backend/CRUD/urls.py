import imp
from django.urls import path
from . import views

urlpatterns = [
    path('index/', views.index.as_view()),
    path('insert/', views.insert.as_view()),
    path('update/<int:pk>', views.update.as_view()),
    path('delete/<int:pk>', views.deletedata),
]
