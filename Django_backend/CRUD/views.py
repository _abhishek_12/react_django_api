import imp
from xmlrpc.client import ResponseError
from django.shortcuts import render
from .serializers import DemoSerializer
from rest_framework import viewsets
from rest_framework.generics import ListAPIView, UpdateAPIView
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.decorators import api_view
from .models import Demo

# Create your views here.

class DemoView(viewsets.ModelViewSet):
    queryset = Demo.objects.all()
    serializer_class = DemoSerializer


class index(APIView):
    def get(self, request):
        queryset = Demo.objects.all()
        serialobj = DemoSerializer(queryset, many=True)
        return Response(serialobj.data)

class insert(APIView):
    def post(self, request):
        serialobj = DemoSerializer(data=request.data)
        if serialobj.is_valid():
            serialobj.save()
            return Response("Data Inserted Successfully")
        else:
            return Response(serialobj.errors)

class update(APIView):
    def get(self, request, pk):
        try:
            queryset = Demo.objects.get(pk = pk)
        except:
            return Response("Data not found")

        serialobj = DemoSerializer(queryset)
        return Response(serialobj.data)

    def post(self, request, pk):
        try:
            queryset = Demo.objects.get(pk = pk)
        except:
            return Response("Data not found")

        serialobj = DemoSerializer(queryset, data=request.data)
        if serialobj.is_valid():
            serialobj.save()
            return Response("Data Updated Successfully")
        else:
            return Response(serialobj.errors)

@api_view(['DELETE'])
def deletedata(request, pk):
    queryset = Demo.objects.get(pk = pk)
    queryset.delete()
    return Response("Data Deleted Successfully")
