import axios from "axios";
import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import './App.css';

const Delete = () => {
    const [del_data, setDel_data] = useState([])
    const { id } = useParams();
    const navigate = useNavigate();

    useEffect(() => {
        LoadData()
    }, [])

    let LoadData = async () =>{
        const { data } = await axios.get(`http://127.0.0.1:8000/demo/update/${ id }`);
        console.log(data)
        setDel_data(data)
    }

    const deleteData = async (id) => {
        await axios.delete(`http://127.0.0.1:8000/demo/delete/${ id }`);
        navigate('/')
    }

    const gohome = async () => {
        navigate('/')
    }

    return (
        <div className='App'>
            <div className="App-header">
                <h3>Are you sure?</h3>
                <h4>You want to delete the following data:</h4>
                <p>First Name: { del_data.first_name }<br />
                Last Name: { del_data.last_name }<br />
                Email: { del_data.email }<br />
                Gender: { del_data.gender }<br />
                IP Address: { del_data.ip_address }</p>
                <button className='btn' onClick={ () => deleteData(del_data.id) }>Yes</button>
                &ensp;
                <button className='btn' onClick={gohome}>No</button>
            </div>
        </div>
    );
};

export default Delete;