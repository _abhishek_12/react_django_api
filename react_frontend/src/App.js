import './App.css';
import React, { Component } from 'react';
import{ BrowserRouter as Router, Route, Routes } from 'react-router-dom';

import View from "./view";
import Insert from "./insert";
import Update from "./update";
import Delete from "./delete";

class App extends Component{
  render() {
    return (
      <div>
        <Router>
          <Routes>
            <Route path='/' element={ <View /> } />
            <Route path='/insert' element={ <Insert />} />
            <Route path='/update/:id' element={ <Update />} />
            <Route path='/delete/:id' element={ <Delete />} />
          </Routes>
        </Router>
      </div>
    );
  }
}

export default App;
