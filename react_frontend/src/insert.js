import './App.css';
import React from "react";
import { useState } from "react";
import axios from "axios";
import { useNavigate } from 'react-router-dom';


function Insert(){
    const navigate = useNavigate();
    const [first_name, setFirst_name] = useState("")
    const [last_name, setLast_name] = useState("")
    const [email, setEmail] = useState("")
    const [gender, setGender] = useState("")
    const [ip_address, setIP_Address] = useState("")

    const InsertData = async () =>{
        let formField = new FormData()
        formField.append('first_name', first_name)
        formField.append('last_name', last_name)
        formField.append('email', email)
        formField.append('gender', gender)
        formField.append('ip_address', ip_address)

        await axios({
            method: 'post',
            url: 'http://127.0.0.1:8000/demo/insert/',
            data: formField
        }).then((response) => {
            console.log(response.data);
            navigate('/')
        })
    }

    return (
        <div className='App'>
            <div className="App-header">
                <form>
                    <div className='row'>
                        <label htmlFor='f_name'>First Name: </label>
                        <input type='text' name="first_name" id="f_name" placeholder="Enter your first name" value={ first_name.first_name } onChange={ (e) => setFirst_name(e.target.value) } />
                    </div>
                    <div className='row'>
                        <label htmlFor='l_name'>Last Name: </label>
                        <input type='text' name="last_name" id="l_name" placeholder="Enter your last name" value={ last_name.last_name } onChange={ (e) => setLast_name(e.target.value) } />
                    </div>
                    <div className='row'>
                        <label htmlFor='em'>Email: </label>
                        <input type='email' name="email" id="em" placeholder="Enter your email" value={ email.email } onChange={ (e) => setEmail(e.target.value) } />
                    </div>
                    <div className='row'>
                        <label htmlFor='gen'>Gender: </label>
                        <input type='text' name="gender" id="gen" placeholder="Enter your gender" value={ gender.gender } onChange={ (e) => setGender(e.target.value) } />
                    </div>
                    <div className='row'>
                        <label htmlFor='ipa'>IP Address: </label>
                        <input type='text' name="ip_address" id="ipa" placeholder="Enter your ip address" value={ ip_address.ip_address } onChange={ (e) => setIP_Address(e.target.value) } />
                    </div>
                    <div className='row'>
                        <button className='btn' onClick={InsertData}>Submit</button>
                        <br />
                    </div>
                </form>
                <button className='btn'><a href='/'>Back</a></button>
            </div>
        </div>
    );
}

export default Insert;