import React from "react";

const MainPage = () => {
    return (
        <div>
            <h3>Welcome to Main Page</h3>
            <small>Just a demo page</small>
        </div>
    );
};

export default MainPage;