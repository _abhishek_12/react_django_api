import './App.css';
import { useState, useEffect } from "react";
import axios from "axios";
import * as React from 'react';

function View(){
    const [Demos, setDemos] = useState([])
    const update = "/update/";
    const del = "/delete/";
    

  useEffect(() => {
    async function getAllDemo(){
      try{
        const demo = await axios.get('http://127.0.0.1:8000/demo/index/')
        console.log(demo.data)
        setDemos(demo.data)
      }
      catch(error){

      }
    }getAllDemo()

  }, [])
  
  return (
    <div className="App">
      <div className='App-header'>
    <h1><u>View Data</u></h1>

    <table border="1" width="100%" cellPadding="10px">
      <thead>
        <tr>
          <th>ID</th>
          <th>First Name</th>
          <th>Last Name</th>
          <th>Email</th>
          <th>Gender</th>
          <th>IP Address</th>
          <th>Operations</th>
        </tr>
      </thead>
      <tbody>
      {
        Demos.map((row, i) => {
          return (
            <tr key={i}>
              <td>{ row.id }</td>
              <td>{ row.first_name }</td>
              <td>{ row.last_name }</td>
              <td>{ row.email }</td>
              <td>{ row.gender }</td>
              <td>{ row.ip_address }</td>
              <td>
                <button className='btn'>
                  <a href={update + row.id}>Edit</a>
                </button>
                &ensp;or&ensp;
                <button className='btn'>
                  <a href={del + row.id}> Delete</a>
                </button>
              </td>
            </tr>
          )
        })
      }
      <tr>
        <td colSpan="7">
          <button className='btn'><a href='/insert/'>Insert Data</a></button>
        </td>
      </tr>
      </tbody>
    </table>
      </div>
  </div>
  )
}

export default View;